%clear all;
    %brick = SimBrick(); % create a SimBrick connection to the simulator
    const
    
    % b = ConnectBrick('BOB');
    
    coastReset(b)
    
    aMax = 40;
    a = aMax + 1;
    
    kbd = KeyWindow();  % create a keyboard entry window      
    while 1
        pause(.1);
        kbd.key
        switch kbd.key
            case 'space'
                stop(b);
            case 'g'
                arm(b,'g');
            case 'u'
                arm(b,'u');
            case 'w'
                b.StopAllMotors();
                cforward(b);
            case 's'
                b.StopAllMotors();
                cbackward(b);
            case 'a'
                b.StopAllMotors();
                left(b, 1);
            case 'd'
                b.StopAllMotors();
                right(b, 1);
            case 'm'
                b.SetColorMode(1, 4);
                while(1)
                    c = color(b);
                    disp(c);
                    if(c == "GREEN")
                       stop(b);
                       break;
                    end
                    if(c == "RED")
                       stop(b);
                       alert(b);
                       pause(2);
                       alert(b);
                       forward(b, 2);
                       alert(b);
                       pause(1);
                       cforward(b);
                    end
                    if(getRWall(b) && a > aMax)
                       alert(b);
                       if(getFWall(b))
                           alert(b);
                           stop(b);
                           backward(b, 3);
                           left(b, 1);
                           cforward(b);
                       end
                    elseif(a > aMax)
                       stop(b);
                       forward(b, 2);
                       right(b, 1);
                       cforward(b);
                       a = 0;
                    end
                    a = a + 1;
                    disp(a);
                end
            case 'q'
                %clear b;
                break;
        end
    end
    kbd.close();        % close the keyboard entry window