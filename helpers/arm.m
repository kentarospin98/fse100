function r = arm(b, task)
    b.WaitForMotor('A');
    b.WaitForMotor('B');
    b.WaitForMotor('C');
    b.WaitForMotor('D');
if (task == 'g')
    b.MoveMotorAngleRel('A', 25, 5, 'Brake');
    b.MoveMotorAngleRel('D', 25, -5, 'Brake');
elseif (task == 'u')
    b.MoveMotorAngleRel('A', 25, -5, 'Coast');
    b.MoveMotorAngleRel('D', 25, 5, 'Coast');
end

r = true;
end