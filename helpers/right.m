function r = right(b, n)

    %b.WaitForMotor('A');
    b.WaitForMotor('B');
    b.WaitForMotor('C');
    %b.WaitForMotor('D');

b.MoveMotorAngleRel('B', 25, -175*n, 'Brake');
b.MoveMotorAngleRel('C', 25, 175*n, 'Brake');

r = true;
end