function r = getRWall(b)
    if(b.UltrasonicDist(2) > 60)
        r = false;
    else
        r = true;
    end
end
