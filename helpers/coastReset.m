function r = coastReset(b)

b.ResetMotorAngle('A');
b.ResetMotorAngle('D');
b.StopMotor('A','Coast');
b.StopMotor('D','Coast');
b.ResetMotorAngle('B');
b.ResetMotorAngle('C');
b.StopMotor('C','Coast');
b.StopMotor('B','Coast');

end