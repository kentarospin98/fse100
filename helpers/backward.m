function r = backward(b, n)

    b.WaitForMotor('A');
    b.WaitForMotor('B');
    b.WaitForMotor('C');
    b.WaitForMotor('D');

b.MoveMotorAngleRel('B', const.SPEED, 180*n, 'Brake');
b.MoveMotorAngleRel('C', const.SPEED, 180*n, 'Brake');

r = true;
end