function g = grab(b, n)
%GRAB Summary of this function goes here
%   Detailed explanation goes here
    b.WaitForMotor('A');
    b.WaitForMotor('B');
    b.WaitForMotor('C');
    b.WaitForMotor('D');
    
    b.MoveMotorAngleRel('B', 25, -180*n, 'Brake');
    b.MoveMotorAngleRel('C', 25, 180*n, 'Brake');
end

