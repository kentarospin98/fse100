%clear all;
    %brick = SimBrick(); % create a SimBrick connection to the simulator
    kbd = KeyWindow();  % create a keyboard entry window      
    while 1
        pause(.1);
        kbd.key
        switch kbd.key
            case 'space'
                stop(b);
            case 'b'
                beep(b);
            case 'g'
                arm(b,'g');
            case 'u'
                arm(b,'u');
            case 'w'
                %b.StopAllMotors();
                cforward(b);
            case 's'
                %b.StopAllMotors();
                cbackward(b);
            case 'a'
                %b.StopAllMotors();
                left(b, 1);
            case 'd'
                %b.StopAllMotors();
                right(b, 1);
            case 'm'
                %b = brick;
%                 clear all;
%                 b = ConnectBrick('BOB');
                main;
            case 'q'
                coastReset(b);
                %clear b;
                break;
        end
    end
    kbd.close();        % close the keyboard entry window